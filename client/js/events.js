"use strict";

Template.form.events({
    'submit form': function (event) {
        event.preventDefault();

        var imageFile = event.currentTarget.children[3].children[0].children[1].files[0];
        var author = event.currentTarget.children[0].children[0].children[0].value;
        var message = event.currentTarget.children[1].children[0].children[0].value;

        if (imageFile === undefined) {
            Materialize.toast('please add an photo', 4000);
            return false;
        } else if (author === "") {
            Materialize.toast('please add a name', 4000);
            return false;
        } else if (message === "") {
            Materialize.toast('please add a comment', 4000);
            return false;
        }

        Collections.Images.insert(imageFile, function (error, fileObject) {
            if (error) {
                Materialize.toast('Somethings wrong...try again', 4000);
                return false;

            } else {
                Collections.Post.insert({
                    name: author,
                    createdAt: new Date(),
                    message: message,
                    ImageID: fileObject._id
                });

                event.currentTarget.children[3].children[0].children[1].files[0] = undefined;
                event.currentTarget.children[0].children[0].children[0].value = "";
                event.currentTarget.children[1].children[0].children[0].value = "";
                $('.grid').masonry('reloadItems');
            }
        });
    }
});